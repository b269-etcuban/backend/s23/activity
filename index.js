let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Bulbasaur"],
    friends: {
        bestFriend: ["Misty", "Brock"],
        rivals: ["Gary"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
};
console.log(trainer);

console.log("Result of Dot Notation:");
console.log(trainer.name);
console.log("Result of Square Bracket Notation:")
console.log(trainer['pokemon']);

trainer.talk();

function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health  = level * 2;
    this.attack = level *  1;
    this.tackle = function(target) {
        target.health -= this.attack;
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s health is now reduced  to " + target.health);
        if (target.health <= 0) {
            console.log(target.name + " has fainted.");
        }
    }
}
let pikachu = new Pokemon('Pikachu', '12');
console.log(pikachu);
let charizard = new Pokemon('Charizard', '8');
console.log(charizard);
let bulbasaur = new Pokemon('Bulbasaur', '100');
console.log(bulbasaur);

pikachu.tackle(charizard)
console.log(charizard)

bulbasaur.tackle(pikachu)
console.log(pikachu)